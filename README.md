HeyCommunity
================================

HeyCommunity 是一个社交软件项目（微信小程序）。   
当前产品功能以（图文、视频）动态分享为核心，支持点赞、评论、回复。同时也支持小程序订阅消息推送。   
将来版本迭代会增加活动、话题、小组、IM 等功能，将来也会开发 APP 和网页端。   

项目官网: https://www.heycommunity.com   
Change Log: https://github.com/HeyCommunity/HeyCommunity/blob/dev-master/CHANGELOG.md

## 产品体验

项目    |   入口   |   描述
-------|----------|----------
微信小程序   | <img src="https://storage.protobia.tech/heycommunity/wxapp-qrcode.jpg" width="100">   | 产品开发中，目前功能包含微信授权登录、发布动态、点赞和评论动态
管理后台    | https://dev.api.heycommunity.com/admin | 产品的管理后台；后台登录帐号密码请联系我们获取


## 项目构建和部署

https://github.com/HeyCommunity/HeyCommunity/wiki


## 支持与联系

方式     |   内容  
-------------|------------------
开发者微信       | <img src="https://storage.protobia.tech/heycommunity/wecom-Rod-qrcode.jpg" height="100"> 
微信交流群       | <img src="https://storage.protobia.tech/heycommunity/wecom-group-qrcode.jpg" height="100">
电子邮箱        | supgeek.rod@gmail.com
